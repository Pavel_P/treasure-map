import pyglet
def change_heart(heart, i):
    if i == 0.5:
        image = pyglet.resource.image('heartHalf.png')
    else:
        image = pyglet.resource.image('heartEmpty.png')
    heart.image = image


def health(superman ,heart_1 , heart_2 ,heart_3 ,heart_4 , heart_5):
    if superman.health == 4.5:
        change_heart(heart_5, 0.5)
    if superman.health == 4:
        change_heart(heart_5, 0)
    if superman.health == 3.5:
        change_heart(heart_4, 0.5)
        change_heart(heart_5, 0)
    if superman.health == 3:
        change_heart(heart_4, 0)
        change_heart(heart_5, 0)
    if superman.health == 2.5:
        change_heart(heart_3, 0.5)
        change_heart(heart_4, 0)
        change_heart(heart_5, 0)
    if superman.health == 2:
        change_heart(heart_3, 0)
        change_heart(heart_4, 0)
        change_heart(heart_5, 0)
    if superman.health == 1.5:
        change_heart(heart_2, 0.5)
        change_heart(heart_3, 0)
        change_heart(heart_4, 0)
        change_heart(heart_5, 0)
    if superman.health == 1:
        change_heart(heart_2, 0)
        change_heart(heart_3, 0)
        change_heart(heart_4, 0)
        change_heart(heart_5, 0)
    if superman.health == 0.5:
        change_heart(heart_1, 0.5)
        change_heart(heart_2, 0)
        change_heart(heart_3, 0)
        change_heart(heart_4, 0)
        change_heart(heart_5, 0)
    if superman.health <=0 :
        change_heart(heart_1, 0)
        change_heart(heart_2, 0)
        change_heart(heart_3, 0)
        change_heart(heart_4, 0)
        change_heart(heart_5, 0)