def bullet_move(bullets ,superman ,guns ,hypot , Blink):
    for i in guns[superman.fone].keys():
        if i < superman.y +20 or bullets[superman.fone][i].x < guns[superman.fone][i].x:
            bullets[superman.fone][i].x -= 8
        if hypot(superman.x - bullets[superman.fone][i].x, superman.y -i) < 20:

            superman.health -= 0.5

            superman.do(Blink(2, 0.2))
            bullets[superman.fone][i].x = guns[superman.fone][i].x
        if bullets[superman.fone][i].x < 1:

            bullets[superman.fone][i].x =guns[superman.fone][i].x