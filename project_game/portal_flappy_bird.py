from pyglet import resource



def portal(dx ,hypot ,superman ,portal_1 ,first_plane ,backgrounds ,main_scene ,RotateBy ,background_portal1 , first_plane_portal ,bird):
    if hypot(superman.x - portal_1.x, superman.y - portal_1.y) < 25 and superman.portal != 2 and superman.fone == 1:
        superman.image = resource.image('alienYellow_walk3.png')
        backgrounds[superman.fone].kill()

        first_plane.kill()
        superman.portal = 1
        main_scene.add(background_portal1)
        main_scene.add(first_plane_portal)
        first_plane_portal.add(bird)

        superman.x = 1
        superman.y = 250


def stone_portal_colliding(dy ,superman , G):
    if superman.portal ==1:
        if superman.y < 71.9:
            superman.y = 72
            superman.Vy = 0
            superman.ay = G
            superman.health = 0

        if superman.y > 428:
            superman.y = 427
            superman.Vy = -20
            superman.ay = 0
            dy = dy * (-1)
            print('stone')
            superman.health = 0

    return dy


def coins_portal_collid(dx, dy ,coins_portal ,superman ,hypot):
    if superman.portal == 1:
        flag = 0
        for i in coins_portal[superman.fone_portal].keys():
            if i - 25 < superman.x + dx < i + 25:

                if (hypot(coins_portal[superman.fone_portal][i].x - superman.x,
                          coins_portal[superman.fone_portal][i].y - superman.y) < 25):
                    flag = i
                    coins_portal[superman.fone_portal][i].kill()

                    superman.money += 1

    return dx, dy, flag


def pipe_colliding_down(dx ,pipes_down ,pipes_up,superman ,G):
    if superman.portal == 1:
        for i in pipes_down[superman.fone_portal].keys():
            if i - 40 < superman.x + dx < i + 40:

                if (pipes_down[superman.fone_portal][i].x - 38 < superman.x + dx < pipes_down[superman.fone_portal][
                    i].x + 38) and (
                        -114 < superman.y - pipes_down[superman.fone_portal][i].y < 114):
                    dx = 0

                    superman.health = 0

                if (pipes_down[superman.fone_portal][i].x - 36 < superman.x + dx < pipes_down[superman.fone_portal][
                    i].x + 36) and superman.y < (pipes_down[superman.fone_portal][i].y + 114):
                    superman.y = pipes_down[superman.fone_portal][i].y + 120
                    superman.Vy = 0
                    superman.ay = -1.1 * G

                    superman.health = 0

        for i in pipes_up[superman.fone_portal].keys():
            if i - 40 < superman.x + dx < i + 40:
                if (pipes_up[superman.fone_portal][i].x - 38 < superman.x + dx < pipes_up[superman.fone_portal][
                    i].x + 38) and (
                        -80 < superman.y - pipes_up[superman.fone_portal][i].y < 100):
                    superman.health = 0

                    dx = 0
                if (pipes_up[superman.fone_portal][i].y - superman.y < 114) and (
                        pipes_up[superman.fone_portal][i].x - 36 < superman.x + dx < pipes_up[superman.fone_portal][
                    i].x + 36) and (
                        superman.Vy > 0):
                    superman.Vy = -20
                    superman.ay = 0

                    superman.health = 0
    return dx