from pyglet import resource

def grass_colliding(dx, dy , grounds , grounds_chocolate ,superman , G ,keyboard ,keys ,backgrounds):
    for i in grounds[superman.fone].keys():

        if i - 40 < superman.x + dx < i + 40:

            if keyboard[keys.W] and (grounds[superman.fone][i].y+45<= superman.y <=grounds[superman.fone][i].y+46):
                superman.Vy=255
                superman.image = resource.image('alienYellow_jump.png')
                superman.ground = True

            if superman.portal == 2 and (grounds[superman.fone][i].x - 35 > superman.x + dx or superman.x + dx > grounds[superman.fone][i].x + 35):
                superman.portal = 0
            if (grounds[superman.fone][i].x - 40 < superman.x + dx < grounds[superman.fone][i].x + 40) and (
                    -22 < superman.y - grounds[superman.fone][i].y < 41):
                if superman.fone == 3 and i == 580 and superman.myst ==False:
                    backgrounds[3].add(grounds[superman.fone][i])
                    superman.myst = True
                dx = 0

            if (grounds[superman.fone][i].x - 35 < superman.x + dx < grounds[superman.fone][
                i].x + 35) and superman.y > (grounds[superman.fone][i].y + 30) and superman.Vy !=255:
                if superman.fone == 3 and i == 580 and superman.myst ==False:
                    superman.myst = True
                    backgrounds[3].add(grounds[superman.fone][i])
                if superman.y < grounds[superman.fone][i].y + 45.9:
                    superman.ground = False
                    superman.y = grounds[superman.fone][i].y + 46
                    superman.Vy = 0
                    superman.ay = G

            if (18 < grounds[superman.fone][i].y - superman.y < 24) and (
                    grounds[superman.fone][i].x - 35 < superman.x + dx < grounds[superman.fone][i].x + 35) and (
                    superman.Vy > 0):
                if superman.fone == 3 and i == 580 and superman.myst ==False:
                    superman.myst = True
                    backgrounds[3].add(grounds[superman.fone][i])
                superman.Vy = -15
                superman.ay = 0
                dy = dy * (-1)
            break

    for i in grounds_chocolate[superman.fone].keys():

        if i - 40 < superman.x + dx < i + 40:

            if keyboard[keys.W] and (grounds_chocolate[superman.fone][i].y+45<= superman.y <=grounds_chocolate[superman.fone][i].y+46):
                superman.Vy=255
                superman.image = resource.image('alienYellow_jump.png')
                superman.ground = True

            if superman.portal == 2 and (grounds_chocolate[superman.fone][i].x - 35 > superman.x + dx or superman.x + dx > grounds_chocolate[superman.fone][i].x + 35):

                superman.portal = 0
            if (grounds_chocolate[superman.fone][i].x - 40 < superman.x + dx < grounds_chocolate[superman.fone][i].x + 40) and (
                    -22 < superman.y - grounds_chocolate[superman.fone][i].y < 41):
                dx = 0

            if (grounds_chocolate[superman.fone][i].x - 35 < superman.x + dx < grounds_chocolate[superman.fone][
                i].x + 35) and superman.y > (grounds_chocolate[superman.fone][i].y + 30) and superman.Vy !=255:
                if superman.y < grounds_chocolate[superman.fone][i].y + 45.9:
                    superman.ground = False
                    superman.y = grounds_chocolate[superman.fone][i].y + 46
                    superman.Vy = 0
                    superman.ay = G

            if (18 < grounds_chocolate[superman.fone][i].y - superman.y < 24) and (
                    grounds_chocolate[superman.fone][i].x - 35 < superman.x + dx < grounds_chocolate[superman.fone][i].x + 35) and (
                    superman.Vy > 0):
                superman.Vy = -15
                superman.ay = 0
                dy = dy * (-1)
            break
    return dx, dy