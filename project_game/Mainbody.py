from cocos.scene import Scene
from cocos.layer import Layer
from cocos.sprite import Sprite
from cocos.director import director
from cocos.actions import *
from math import hypot
from cocos.layer import *
import pyglet
from coins import *
from cocos.menu import *
from fones import *
from grass_collid import *
from spike_collid import *
from health import *
from enemy import *
from board import *
from bullet import *
from cocos.scenes.transitions import *
from cocos.actions import *
from cocos.sprite import *
from cocos.menu import *
from cocos.text import *


from gun import *
from grass_danger_collid import *
from money_display import *
from portal_flappy_bird import *

def main():


    G = 9.81 * 64 / 1.5

    class KeyboardController(Action):

        def step(self, dt):
            if superman.health <= 4 and superman.portal !=1:
                first_plane.add(lose)
                superman.stop()
                first_plane.do(Delay(3) + CallFunc(director.scene.end))



            if superman.health <= 0 and superman.portal ==1:
                first_plane_portal.add(lose)

                first_plane_portal.do(Delay(3) + CallFunc(director.scene.end))

            if  superman.money==40 and superman.fone==3 and superman.x>710 and superman.y<200 :
                first_plane.add(win)
                first_plane.do(Delay(3) + CallFunc(director.scene.end))



            health(superman, heart_1, heart_2, heart_3, heart_4, heart_5)
            if superman.portal !=1:
                superman.direction = keyboard[keys.D] - keyboard[keys.A]
                superman.ax = superman.direction * 100 - superman.Vx

                dx = superman.Vx * dt
                dVx = superman.ax * dt
                dy = superman.Vy * dt
                dVy = (superman.ay - G) * dt

                if keyboard[keys.W] and (70 <= superman.y <= 72.1):
                    superman.image = pyglet.resource.image('alienYellow_jump.png')
                    superman.ground=True
                    superman.Vy = 255
                else:
                    superman.ay = 0

                if superman.y < 71.9:
                    superman.ground = False
                    superman.y = 72
                    superman.Vy = 0
                    superman.ay = G

                if superman.x > 740 and superman.fone!=3:
                    superman.x = 2
                    fones(1, backgrounds, superman, first_plane, main_scene)
                if superman.x > 740 and superman.fone==3 and superman.y>100:
                    dx = 0
                    superman.x -= 1
                    superman.Vx = -60

                if superman.x < 1 and superman.fone !=0 and superman.fone !=3:
                    superman.x = 740
                    fones(-1, backgrounds, superman, first_plane, main_scene)
                if superman.x < 1 and superman.fone ==0:
                    dx=0
                    superman.x+=1
                    superman.Vx=60
                if superman.x < 1 and superman.fone ==3:
                    if superman.y>90:
                        dx=0
                        superman.x+=1
                        superman.Vx=60
                    else:
                        superman.x = 740
                        fones(-1, backgrounds, superman, first_plane, main_scene)
                if superman.ground == False:
                    if keyboard[keys.D]:
                        if superman.image != animation_right:
                            superman.image = animation_right
                    elif keyboard[keys.A]:
                        if superman.image != animation_left:
                            superman.image = animation_left


                    else:
                        superman.image = pyglet.resource.image('alienYellow_stand.png')

                dx, dy = grass_colliding(dx, dy, grounds, grounds_chocolate , superman, G ,keyboard ,keys ,backgrounds)

                dx, dy = spike_colliding(dx, dy, spikes, superman, Blink)
                dx, dy = board_colliding(dx, dy ,boards ,superman)
                dx, dy ,fl = grass_danger_colliding(dx, dy, grounds_danger, superman, G, keyboard, keys, Blink ,hypot)

                dx, dy, mon = coins_colliding(dx, dy, coins, superman, hypot)
                if fl != 0:

                    grounds_danger[superman.fone].pop(fl)
                if mon != 0:
                    coins[superman.fone].pop(mon)
                dx, dy, i = enemy_colliding(dx, dy, enemies, superman, MoveTo, Blink)
                if i != 0:
                    enemies[superman.fone].pop(i)
                money(superman, number_1, number_2)
                dx,dy=gun_colliding(dx, dy ,keyboard ,keys ,superman ,guns ,bullets)
                bullet_move(bullets ,superman ,guns ,hypot , Blink)
                portal(dx ,hypot ,superman ,portal_1 ,first_plane ,backgrounds ,main_scene ,RotateBy ,background_portal1 , first_plane_portal ,bird)
                dy =superman.Vy*dt
                superman.x += dx
                superman.Vx += 2 * dVx
                superman.y += dy
                superman.Vy += dVy


            else:
                dx = 0.8

                dy = superman.Vy * dt
                dVy = (superman.ay - G) * dt
                dx, dy, mon = coins_portal_collid(dx, dy ,coins_portal ,superman ,hypot)
                if mon != 0:
                    coins_portal[superman.fone_portal].pop(mon)
                money(superman, number_1, number_2)
                if superman.health !=0 :
                    if keyboard[keys.SPACE]:
                        superman.image = animation_fly
                        superman.Vy = 180
                        
                        keyboard[keys.SPACE] = False
                    else:

                        superman.ay = 0
                    dx = pipe_colliding_down(dx ,pipes_down ,pipes_up,superman ,G)
                    superman.x += dx
                if superman.fone_portal ==1 and superman.x >600:
                    superman.x=portal_1.x
                    superman.y = 500
                    superman.image = pyglet.resource.image('alienYellow_stand.png')
                    superman.Vy=0
                    superman.Vx=0
                    background_portal2.kill()
                    first_plane_portal.kill()
                    superman.portal = 2
                    superman.fone_portal=0
                    main_scene.add(backgrounds[superman.fone])
                    main_scene.add(first_plane)

                if superman.x>740:
                    superman.x = 0
                    superman.fone_portal =1
                    background_portal1.kill()
                    first_plane_portal.kill()
                    main_scene.add(background_portal2)

                    main_scene.add(first_plane_portal)
                dy = stone_portal_colliding(dy ,superman , G)
                superman.y += dy
                superman.Vy += dVy

    class OptionsMenu(Menu):
        def __init__(self):
            super(OptionsMenu, self).__init__('COLLECTOR')

            # you can override the font that will be used for the title and the items
            self.font_title['font_name'] = 'Edit Undo Line BRK'
            self.font_title['font_size'] = 72
            self.font_title['color'] = (204, 164, 164, 255)

            self.font_item['font_name'] = 'Edit Undo Line BRK',
            self.font_item['color'] = (32, 16, 32, 255)
            self.font_item['font_size'] = 32
            self.font_item_selected['font_name'] = 'Edit Undo Line BRK'
            self.font_item_selected['color'] = (32, 16, 32, 255)
            self.font_item_selected['font_size'] = 46

            # you can also override the font size and the colors. see menu.py for
            # more info

            # example: menus can be vertical aligned and horizontal aligned
            self.menu_anchor_y = CENTER
            self.menu_anchor_x = CENTER

            self.items = []

            self.lang =['En' ]


            self.items.append(ToggleMenuItem('Show FPS:', self.on_show_fps, director.show_FPS))
            self.items.append(MultipleMenuItem('Language: ', self.language ,self.lang))
            self.items.append(MenuItem('Back', self.on_quit))

            self.create_menu(self.items, shake(), shake_back())


        def language(self ,a):

            director.replace(menu_scene_rus)

        def on_quit(self):
            self.parent.switch_to(0)



        def on_show_fps(self, value):
            director.show_FPS = value

    class MainMenu(Menu):

        def __init__(self):
            super(MainMenu, self).__init__('COLLECTOR')

            self.font_title['font_name'] = 'Edit Undo Line BRK'
            self.font_title['font_size'] = 72
            self.font_title['color'] = (204, 164, 164, 255)

            self.font_item['font_name'] = 'Edit Undo Line BRK',
            self.font_item['color'] = (32, 16, 32, 255)
            self.font_item['font_size'] = 32
            self.font_item_selected['font_name'] = 'Edit Undo Line BRK'
            self.font_item_selected['color'] = (32, 16, 32, 255)
            self.font_item_selected['font_size'] = 46

            # example: menus can be vertical aligned and horizontal aligned
            self.menu_anchor_y = CENTER
            self.menu_anchor_x = CENTER

            items = []

            items.append(MenuItem('Game', self.on_game))
            items.append(MenuItem('Options', self.on_options))
            items.append(MenuItem('Rules', self.on_rules))
            items.append(MenuItem('Quit', self.on_quit))
            self.create_menu(items, shake(), shake_back())

        def on_game(self):
            director.push(main_scene)

        def on_options(self):
            self.parent.switch_to(1)

        def on_rules(self):
            self.parent.switch_to(2)
            menu_scene.add(back_rule)


        def on_quit(self):
            pyglet.app.exit()

    class OptionsMenu_rus(Menu):
        def __init__(self):
            super(OptionsMenu_rus, self).__init__('COLLECTOR')
            self.font_title['font_name'] = 'Edit Undo Line BRK'
            self.font_title['font_size'] = 72
            self.font_title['color'] = (204, 164, 164, 255)

            self.font_item['font_name'] = 'Edit Undo Line BRK',
            self.font_item['color'] = (32, 16, 32, 255)
            self.font_item['font_size'] = 32
            self.font_item_selected['font_name'] = 'Edit Undo Line BRK'
            self.font_item_selected['color'] = (32, 16, 32, 255)
            self.font_item_selected['font_size'] = 46
            self.menu_anchor_y = CENTER
            self.menu_anchor_x = CENTER

            self.items = []
            self.lang = ['Ру']


            self.items.append(ToggleMenuItem('Показывать FPS: ', self.on_show_fps, director.show_FPS))
            self.items.append(MultipleMenuItem('Язык: ', self.language, self.lang))
            self.items.append(MenuItem('Назад', self.on_quit))

            self.create_menu(self.items, shake(), shake_back())



        def language(self, a):

            director.replace(menu_scene)

        def on_quit(self):
            self.parent.switch_to(0)

        def on_show_fps(self, value):
            director.show_FPS = value

    class MainMenu_rus(Menu):

        def __init__(self):
            super(MainMenu_rus, self).__init__('COLLECTOR')

            self.font_title['font_name'] = 'Edit Undo Line BRK'
            self.font_title['font_size'] = 72
            self.font_title['color'] = (204, 164, 164, 255)

            self.font_item['font_name'] = 'Edit Undo Line BRK',
            self.font_item['color'] = (32, 16, 32, 255)
            self.font_item['font_size'] = 32
            self.font_item_selected['font_name'] = 'Edit Undo Line BRK'
            self.font_item_selected['color'] = (32, 16, 32, 255)
            self.font_item_selected['font_size'] = 46

            # example: menus can be vertical aligned and horizontal aligned
            self.menu_anchor_y = CENTER
            self.menu_anchor_x = CENTER

            items = []

            items.append(MenuItem('Игра', self.on_game))
            items.append(MenuItem('Настройки', self.on_options))
            items.append(MenuItem('Правила', self.on_rules))
            items.append(MenuItem('Выход', self.on_quit))

            self.create_menu(items, shake(), shake_back())

        def on_game(self):
            director.push(main_scene)

        def on_options(self):
            self.parent.switch_to(1)

        def on_rules(self):
            self.parent.switch_to(2)
            menu_scene_rus.add(back_rule)

        def on_quit(self):
            pyglet.app.exit()


    class ScoresLayer(Menu):
        def __init__(self):
            super(ScoresLayer, self).__init__('COLLECTOR')
            self.items = []
            self.items.append(MenuItem('Back', self.on_q))
            self.create_menu(self.items, shake(), shake_back())
        def on_q(self):
            4
        def on_quit(self):
            self.parent.switch_to(0)
            back_rule.kill()




    director.init(width=750, height=500, autoscale=None)
    from pyglet.window import key as keys

    keyboard = keys.KeyStateHandler()
    director.window.push_handlers(keyboard)

    main_scene = Scene()
    first_plane = Layer()
    background_1 = Layer()
    background_2 = Layer()
    background_3 = Layer()
    background_4 = Layer()
    first_plane_portal = Layer()
    backgrounds = []

    background_portal1 = Layer()
    background_portal2 = Layer()

    backgrounds.append(background_1)
    backgrounds.append(background_2)
    backgrounds.append(background_3)
    backgrounds.append(background_4)

    sup = Sprite("alienYellow_walk1.png", scale=0.5, position=(568, 95 + 700), color=(0, 255, 0))

    im_sup_right = ('alienYellow_walk2.png', 'alienYellow_walk1.png')
    im_sup_rightes = map(lambda img: image.load(img), im_sup_right)
    animation_right = image.Animation.from_image_sequence(im_sup_rightes, period=0.2, loop=True)

    im_sup_left = ('alienYellow_walk2_left.png', 'alienYellow_walk1_left.png')
    im_sup_leftes = map(lambda img: image.load(img), im_sup_left)
    animation_left = image.Animation.from_image_sequence(im_sup_leftes, period=0.2, loop=True)

    im_sup_fly = ('alienYellow_walk5.png', 'alienYellow_walk6.png')
    im_sup_flies = map(lambda img: image.load(img), im_sup_fly)
    animation_fly = image.Animation.from_image_sequence(im_sup_flies, period=0.2, loop=False)

    superman = Sprite("alienYellow_stand.png", scale=0.5, position=(30, 250))                                                                              #Создание главных героев

    sp_1 = Sprite("spikes.png", scale=0.7, position=(430, 290))
    sp_2 = Sprite("spikes.png", scale=0.7, position=(300, 77))
    sp_3 = Sprite("spikes.png", scale=0.7, position=(630, 77))
    sp_4 = Sprite("spikes_down.png", scale=0.7, position=(182, 262))
    sp_5 = Sprite("spikes.png", scale=0.7, position=(580, 320))

    sp_6 = Sprite("spikes.png", scale=0.7, position=(130, 77))
    sp_7 = Sprite("spikes.png", scale=0.7, position=(500, 77))
    sp_8 = Sprite("spikes.png", scale=0.7, position=(580, 77))
    sp_9 = Sprite("spikes_down.png", scale=0.7, position=(180, 363))
    sp_10 = Sprite("spikes.png", scale=0.7, position=(680, 77))
    sp_11 = Sprite("spikes.png", scale=0.7, position=(360, 77))
    sp_12 = Sprite("spikes.png", scale=0.7, position=(230, 77))
    sp_13 = Sprite("spikes.png", scale=0.7, position=(290, 77))

    sp_14 = Sprite("spikes.png", scale=0.7, position=(727, 210))
    sp_15 = Sprite("spikes.png", scale=0.7, position=(530, 77))
    sp_16 = Sprite("spikes.png", scale=0.7, position=(605, 210 + 10))
    sp_17 = Sprite("spikes.png", scale=0.7, position=(360, 77))
    sp_18 = Sprite("spikes.png", scale=0.7, position=(210, 77))
    sp_19 = Sprite("spikes.png", scale=0.7, position=(281, 240))


    cn_1 = Sprite("coins.png", scale=0.4, position=(115, 160), color=(255, 255, 0))
    cn_2 = Sprite("coins.png", scale=0.4, position=(360, 440), color=(255, 255, 0))
    cn_3 = Sprite("coins.png", scale=0.4, position=(650, 440), color=(255, 255, 0))
    cn_4 = Sprite("coins.png", scale=0.4, position=(500, 440), color=(255, 255, 0))
    cn_5 = Sprite("coins.png", scale=0.4, position=(240, 89), color=(255, 255, 0))
    cn_6 = Sprite("coins.png", scale=0.4, position=(580, 85), color=(255, 255, 0))
    cn_7 = Sprite("coins.png", scale=0.4, position=(420, 290), color=(255, 255, 0))
    cn_8 = Sprite("coins.png", scale=0.4, position=(470, 290), color=(255, 255, 0))
    cn_9 = Sprite("coins.png", scale=0.4, position=(530, 290), color=(255, 255, 0))
    cn_10 = Sprite("coins.png", scale=0.4, position=(590, 290), color=(255, 255, 0))
    cn_11 = Sprite("coins.png", scale=0.4, position=(190 + 100, 120), color=(255, 255, 0))
    cn_12 = Sprite("coins.png", scale=0.4, position=(540, 120), color=(255, 255, 0))
    cn_29 = Sprite("coins.png", scale=0.4, position=(480, 85), color=(255, 255, 0))
    cn_30 = Sprite("coins.png", scale=0.4, position=(540, 85), color=(255, 255, 0))
    cn_31 = Sprite("coins.png", scale=0.4, position=(420, 85), color=(255, 255, 0))
    cn_32 = Sprite("coins.png", scale=0.4, position=(1, 85), color=(255, 255, 0))

    cn_33 = Sprite("coins.png", scale=0.4, position=(400, 330), color=(255, 255, 0))
    cn_34 = Sprite("coins.png", scale=0.4, position=(500, 220), color=(255, 255, 0))
    cn_35 = Sprite("coins.png", scale=0.4, position=(360, 480), color=(255, 255, 0))
    cn_36 = Sprite("coins.png", scale=0.4, position=(650, 85), color=(255, 255, 0))
    cn_37 = Sprite("coins.png", scale=0.4, position=(142, 325), color=(255, 255, 0))
    cn_38 = Sprite("coins.png", scale=0.4, position=(220, 240), color=(255, 255, 0))

    cn_39 = Sprite("coins.png", scale=0.4, position=(100, 250), color=(255, 255, 0))
    cn_40 = Sprite("coins.png", scale=0.4, position=(387, 305), color=(255, 255, 0))
    cn_41 = Sprite("coins.png", scale=0.4, position=(425, 486), color=(255, 255, 0))
    cn_42 = Sprite("coins.png", scale=0.4, position=(150, 85), color=(255, 255, 0))
    cn_43 = Sprite("coins.png", scale=0.4, position=(666, 205), color=(255, 255, 0))
    cn_44 = Sprite("coins.png", scale=0.4, position=(430, 85), color=(255, 255, 0))

    cn_13 = Sprite("coins.png", scale=0.4, position=(50, 215), color=(255, 255, 0))
    cn_14 = Sprite("coins.png", scale=0.4, position=(140, 220), color=(255, 255, 0))
    cn_15 = Sprite("coins.png", scale=0.4, position=(220, 270), color=(255, 255, 0))
    cn_16 = Sprite("coins.png", scale=0.4, position=(280, 325), color=(255, 255, 0))
    cn_17 = Sprite("coins.png", scale=0.4, position=(350, 160), color=(255, 255, 0))
    cn_18 = Sprite("coins.png", scale=0.4, position=(420, 220), color=(255, 255, 0))
    cn_19 = Sprite("coins.png", scale=0.4, position=(560, 150), color=(255, 255, 0))
    cn_20 = Sprite("coins.png", scale=0.4, position=(710, 400), color=(255, 255, 0))

    cn_21 = Sprite("coins.png", scale=0.4, position=(100, 140), color=(255, 255, 0))
    cn_22 = Sprite("coins.png", scale=0.4, position=(190, 240), color=(255, 255, 0))
    cn_23 = Sprite("coins.png", scale=0.4, position=(280, 220), color=(255, 255, 0))
    cn_24 = Sprite("coins.png", scale=0.4, position=(420, 170), color=(255, 255, 0))
    cn_25 = Sprite("coins.png", scale=0.4, position=(560, 345), color=(255, 255, 0))
    cn_26 = Sprite("coins.png", scale=0.4, position=(0, 89), color=(255, 255, 0))
    cn_27 = Sprite("coins.png", scale=0.4, position=(0, 120), color=(255, 255, 0))
    cn_28 = Sprite("coins.png", scale=0.4, position=(0, 120), color=(255, 255, 0))

    portal_1 = Sprite("portal.png", scale=0.1, position=(180, 170 + 300 - 10))
    portal_1_end = Sprite("portal.png", scale=0.5, position=(670, 300))                                                                            #Создание порталов
    en_1 = Sprite("alienBlue_walk1.png", scale=0.5, position=(480, 74))
    en_2 = Sprite("alienBlue_walk1.png", scale=0.5, position=(500, 287))
    en_3 = Sprite("alienBlue_walk1.png", scale=0.5, position=(550, 72))

    gr_1 = Sprite("grassHalf.png", scale=0.7, position=(115, 110))
    gr_2 = Sprite("grassHalf.png", scale=0.7, position=(250, 160))
    gr_3 = Sprite("grassHalf.png", scale=0.7, position=(360, 390))
    gr_4 = Sprite("grassHalf.png", scale=0.7, position=(500, 390))
    gr_5 = Sprite("grassHalf.png", scale=0.7, position=(650, 390))
    gr_6 = Sprite("grassHalf.png", scale=0.7, position=(720, 390))
    gr_7 = Sprite("grassHalf.png", scale=0.7, position=(430, 240))
    gr_8 = Sprite("grassHalf.png", scale=0.7, position=(180, 110))
    gr_9 = Sprite("grassHalf.png", scale=0.7, position=(60, 180))
    gr_10 = Sprite("grassHalf.png", scale=0.7, position=(425, 240))
    gr_3_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(480, 240))
    gr_12 = Sprite("grassHalf.png", scale=0.7, position=(310, 320))
    gr_4_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(590, 240))
    gr_14 = Sprite("grassHalf.png", scale=0.7, position=(535, 240))
    gr_15 = Sprite("grassHalf.png", scale=0.7, position=(140, 110))
    gr_16 = Sprite("grassHalf.png", scale=0.7, position=(220, 190))
    gr_17 = Sprite("grassHalf.png", scale=0.7, position=(720, 100))
    gr_18 = Sprite("grassHalf.png", scale=0.7, position=(500, 355))
    gr_19 = Sprite("grassHalf.png", scale=0.7, position=(350, 430))

    gr_20 = Sprite("grassHalf.png", scale=0.7, position=(100, 200))
    gr_21 = Sprite("grassHalf.png", scale=0.7, position=(180, 270))
    gr_22 = Sprite("grassHalf.png", scale=0.7, position=(281, 190))
    gr_23 = Sprite("grassHalf.png", scale=0.7, position=(385, 245))
    gr_24 = Sprite("grassHalf.png", scale=0.7, position=(490, 320))
    gr_25 = Sprite("grassHalf.png", scale=0.7, position=(580, 400))

    gr_1_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(182, 291))
    gr_2_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(580, 270))
    gr_5_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(180, 390))
    gr_6_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(220, 358))
    gr_7_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(130, 275))
    gr_8_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(500, 160))
    gr_9_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(400, 275))
    gr_10_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(605, 160 + 10))
    gr_11_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(421, 450))
    gr_26 = Sprite("chocoHalf.png", scale=0.7, position=(665, 160))
    gr_13_chocolate = Sprite("chocoHalf.png", scale=0.7, position=(726, 160))

    number_1 = Sprite("hud_0.png", scale=0.7, position=(15, 485))
    number_2 = Sprite("hud_0.png", scale=0.7, position=(35, 485))                                                                             #Создание монет

    pp_1_down = Sprite("flappy_bird.png", scale=0.15, position=(140, 80))
    pp_2_down = Sprite("flappy_bird.png", scale=0.15, position=(280, 180))
    pp_3_down = Sprite("flappy_bird.png", scale=0.15, position=(420, 80))
    pp_4_down = Sprite("flappy_bird.png", scale=0.15, position=(560, 0))
    pp_5_down = Sprite("flappy_bird.png", scale=0.15, position=(715, 200))
    pp_6_down = Sprite("flappy_bird.png", scale=0.15, position=(280, 80))
    pp_7_down = Sprite("flappy_bird.png", scale=0.15, position=(420, 30))
    pp_8_down = Sprite("flappy_bird.png", scale=0.15, position=(560, 200))

    pp_1_up = Sprite("flappy_bird_2.png", scale=0.15, position=(140, 360 + 30))
    pp_2_up = Sprite("flappy_bird_2.png", scale=0.15, position=(280, 480 + 30))
    pp_3_up = Sprite("flappy_bird_2.png", scale=0.15, position=(420, 360 + 30))
    pp_4_up = Sprite("flappy_bird_2.png", scale=0.15, position=(560, 300 + 30))
    pp_5_up = Sprite("flappy_bird_2.png", scale=0.15, position=(100, 360 + 30))
    pp_6_up = Sprite("flappy_bird_2.png", scale=0.15, position=(280, 380 + 30))
    pp_7_up = Sprite("flappy_bird_2.png", scale=0.15, position=(420, 330 + 30))
    pp_8_up = Sprite("flappy_bird_2.png", scale=0.15, position=(560, 480 + 30))


    heart_1 = Sprite("heartFull.png", scale=0.4, position=(60, 485))
    heart_2 = Sprite("heartFull.png", scale=0.4, position=(80, 485))
    heart_3 = Sprite("heartFull.png", scale=0.4, position=(100, 485))
    heart_4 = Sprite("heartFull.png", scale=0.4, position=(120, 485))
    heart_5 = Sprite("heartFull.png", scale=0.4, position=(140, 485))                                                      #Создание сердец


    sign_exit =Sprite("signExit.png", scale=0.7, position=(720, 74))
    sign_right = Sprite("signRight.png", scale=0.6, position=(730, 74))

    brd_1 = Sprite("boardDown.png", scale=0.7, position=(250, 208))
    brd_2 = Sprite("boardDown.png", scale=0.7, position=(80, 77))
    brd_3 = Sprite("boardDown.png", scale=0.7, position=(280, 77))

    picture = Sprite("bg.png", position=(375, 250))
    gn_1 = Sprite("gun.png",scale=0.7, position=(720, 438))
    bul_1 = Sprite("bullet.png",scale=0.01, position=(gn_1.x, gn_1.y))
    gn_2 = Sprite("gun.png", scale=0.7, position=(720, 255))
    bul_2 = Sprite("bullet.png", scale=0.01, position=(gn_2.x, gn_2.y))
    gn_3 = Sprite("gun.png", scale=0.7, position=(720, 150))
    bul_3 = Sprite("bullet.png", scale=0.01, position=(gn_3.x, gn_3.y))
    gn_4 = Sprite("gun.png", scale=0.7, position=(720, 335))
    bul_4 = Sprite("bullet.png", scale=0.01, position=(gn_4.x, gn_4.y))
    gn_5 = Sprite("gun.png", scale=0.7, position=(720, 415))
    bul_5 = Sprite("bullet.png", scale=0.01, position=(gn_5.x, gn_5.y))
    gn_6 = Sprite("gun.png", scale=0.7, position=(720, 480))
    bul_6 = Sprite("bullet.png", scale=0.01, position=(gn_6.x, gn_6.y))

    gr_danger_1 = Sprite("box_danger.png", scale=0.7, position=(500, 130))
    gr_danger_2 = Sprite("box_danger.png", scale=0.7, position=(170, 257))
    picture.scale_x = 3
    picture.scale_y = 2
    pp_1_down.scale_y =2.5
    pp_2_down.scale_y = 2.5
    pp_3_down.scale_y = 2.5
    pp_4_down.scale_y = 2.5
    pp_5_down.scale_y = 2.5
    pp_6_down.scale_y = 2.5
    pp_7_down.scale_y = 2.5
    pp_8_down.scale_y = 2.5
    pp_1_up.scale_y = 2.5
    pp_2_up.scale_y = 2.5
    pp_3_up.scale_y = 2.5
    pp_4_up.scale_y = 2.5
    pp_5_up.scale_y = 2.5
    pp_6_up.scale_y = 2.5
    pp_7_up.scale_y = 2.5
    pp_8_up.scale_y = 2.5
    superman.Vy = 0
    superman.ay = 0
    superman.Vx = 0
    superman.ax = 0
    superman.fone = 0
    superman.health = 5
    superman.money = 0
    superman.portal=0
    superman.menu =True
    superman.fone_portal = 0
    superman.myst =False
    superman.box_danger = False
    superman.ground = False
    superman.language ='eng'

    backgrounds[0].add(picture)
    backgrounds[1].add(picture)
    backgrounds[2].add(picture)
    backgrounds[3].add(picture)
    background_portal1.add(picture)
    background_portal2.add(picture)
    superman.do(KeyboardController())
    

    ground_1 = dict()
    ground_2 = dict()
    ground_3 = dict()
    ground_4 = dict()
    grounds = [ground_1, ground_2, ground_3 ,ground_4]

    ground_1[gr_1.x] = gr_1
    ground_1[gr_2.x] = gr_2
    ground_1[gr_3.x] = gr_3
    ground_1[gr_4.x] = gr_4
    ground_1[gr_5.x] = gr_5
    ground_1[gr_6.x] = gr_6
    ground_1[gr_7.x] = gr_7

    ground_2[gr_8.x] = gr_8
    ground_2[gr_9.x] = gr_9
    ground_2[gr_10.x] = gr_10
    ground_2[gr_12.x] = gr_12
    ground_2[gr_14.x] = gr_14

    ground_3[gr_15.x] = gr_15
    ground_3[gr_16.x] = gr_16
    ground_3[gr_17.x] = gr_17
    ground_3[gr_18.x] = gr_18
    ground_3[gr_19.x] = gr_19

    ground_4[gr_20.x] = gr_20
    ground_4[gr_21.x] = gr_21
    ground_4[gr_22.x] = gr_22
    ground_4[gr_23.x] = gr_23
    ground_4[gr_24.x] = gr_24
    ground_4[gr_25.x] = gr_25
    ground_4[gr_26.x] = gr_26

    ground_1_chocolate = dict()
    ground_2_chocolate = dict()
    ground_3_chocolate = dict()
    ground_4_chocolate = dict()
    grounds_chocolate = [ground_1_chocolate, ground_2_chocolate, ground_3_chocolate ,ground_4_chocolate]

    ground_1_chocolate[gr_1_chocolate.x] = gr_1_chocolate
    ground_1_chocolate[gr_2_chocolate.x] = gr_2_chocolate
    ground_2_chocolate[gr_3_chocolate.x] = gr_3_chocolate
    ground_2_chocolate[gr_4_chocolate.x] = gr_4_chocolate
    ground_2_chocolate[gr_5_chocolate.x] = gr_5_chocolate
    ground_3_chocolate[gr_6_chocolate.x] = gr_6_chocolate
    ground_3_chocolate[gr_7_chocolate.x] = gr_7_chocolate
    ground_3_chocolate[gr_8_chocolate.x] = gr_8_chocolate
    ground_3_chocolate[gr_9_chocolate.x] = gr_9_chocolate
    ground_4_chocolate[gr_10_chocolate.x] = gr_10_chocolate
    ground_4_chocolate[gr_11_chocolate.x] = gr_11_chocolate
    ground_4_chocolate[gr_13_chocolate.x] = gr_13_chocolate

    ground_danger_1 = dict()
    ground_danger_2 = dict()
    ground_danger_3 = dict()
    ground_danger_4 = dict()
    grounds_danger = [ground_danger_1, ground_danger_2, ground_danger_3 ,ground_danger_4]

    ground_danger_1 [gr_danger_1.x] = gr_danger_1
    ground_danger_2[gr_danger_2.x] = gr_danger_2


    spike_1 = dict()
    spike_2 = dict()
    spike_3 = dict()
    spike_4 = dict()
    spikes = [spike_1, spike_2, spike_3 ,spike_4]
    spike_1[sp_1.x] = sp_1
    spike_1[sp_2.x] = sp_2
    spike_1[sp_3.x] = sp_3
    spike_1[sp_4.x] = sp_4
    spike_1[sp_5.x] = sp_5

    spike_2[sp_6.x] = sp_6
    spike_2[sp_7.x] = sp_7
    spike_2[sp_8.x] = sp_8
    spike_2[sp_9.x] = sp_9
    spike_2[sp_10.x] = sp_10

    spike_3[sp_11.x] = sp_11
    spike_3[sp_12.x] = sp_12
    spike_3[sp_13.x] = sp_13

    spike_4[sp_14.x] = sp_14
    spike_4[sp_15.x] = sp_15
    spike_4[sp_16.x] = sp_16
    spike_4[sp_17.x] = sp_17
    spike_4[sp_18.x] = sp_18
    spike_4[sp_19.x] = sp_19

    enemy_1 = dict()
    enemy_2 = dict()
    enemy_3 = dict()
    enemy_4= dict()

    enemies = [enemy_1, enemy_2 , enemy_3 ,enemy_4]
    enemy_1[en_1.x] = en_1
    enemy_2[en_2.x] = en_2
    enemy_3[en_3.x] = en_3

    gun_1 = dict()
    gun_2 = dict()
    gun_3 = dict()
    gun_4 = dict()
    guns = [gun_1 , gun_2 ,gun_3 ,gun_4]
    gun_1[gn_1.y]=gn_1
    gun_3[gn_2.y] = gn_2
    gun_3[gn_3.y] = gn_3
    gun_3[gn_4.y] = gn_4
    gun_3[gn_5.y] = gn_5
    gun_3[gn_6.y] = gn_6

    bullet_1 =dict()
    bullet_2 = dict()
    bullet_3 = dict()
    bullet_4 = dict()
    bullets = [bullet_1, bullet_2, bullet_3 ,bullet_4]
    bullet_1[gn_1.y] =bul_1
    bullet_3[gn_2.y] = bul_2
    bullet_3[gn_3.y] = bul_3
    bullet_3[gn_4.y] = bul_4
    bullet_3[gn_5.y] = bul_5
    bullet_3[gn_6.y] = bul_6

    board_1 = dict()
    board_2 = dict()
    board_3 = dict()
    board_4 = dict()
    boards = [board_1 , board_2 ,board_3 ,board_4]
    board_1[brd_1.x]=brd_1
    board_4[brd_2.x] = brd_2
    board_4[brd_3.x] = brd_3

    coin_1 = dict()
    coin_2 = dict()
    coin_3 = dict()
    coin_4 = dict()
    coins = [coin_1,coin_2,coin_3 ,coin_4]
    coin_1[cn_1.x] = cn_1
    coin_1[cn_2.x] = cn_2
    coin_1[cn_3.x] = cn_3
    coin_1[cn_4.x] = cn_4
    coin_1[cn_5.x] = cn_5


    coin_1[cn_29.x] = cn_29
    coin_1[cn_30.x] = cn_30
    coin_1[cn_31.x] = cn_31



    coin_2[cn_7.x] = cn_7
    coin_2[cn_8.x] = cn_8
    coin_2[cn_9.x] = cn_9
    coin_2[cn_10.x] = cn_10
    coin_2[cn_11.x] = cn_11
    coin_2[cn_12.x] = cn_12

    coin_3[cn_33.x] = cn_33
    coin_3[cn_34.x] = cn_34
    coin_3[cn_35.x] = cn_35
    coin_3[cn_36.x] = cn_36
    coin_3[cn_37.x] = cn_37
    coin_3[cn_38.x] = cn_38

    coin_4[cn_39.x] = cn_39
    coin_4[cn_40.x] = cn_40
    coin_4[cn_41.x] = cn_41
    coin_4[cn_42.x] = cn_42
    coin_4[cn_43.x] = cn_43
    coin_4[cn_44.x] = cn_44

    coin_1_portal = dict()
    coin_2_portal = dict()
    coins_portal = [coin_1_portal, coin_2_portal]
    coin_1_portal[cn_13.x] = cn_13
    coin_1_portal[cn_14.x] = cn_14
    coin_1_portal[cn_15.x] = cn_15
    coin_1_portal[cn_16.x] = cn_16
    coin_1_portal[cn_17.x] = cn_17
    coin_1_portal[cn_18.x] = cn_18
    coin_1_portal[cn_19.x] = cn_19
    coin_1_portal[cn_20.x] = cn_20

    coin_2_portal[cn_21.x] = cn_21
    coin_2_portal[cn_22.x] = cn_22
    coin_2_portal[cn_23.x] = cn_23
    coin_2_portal[cn_24.x] = cn_24
    coin_2_portal[cn_25.x] = cn_25




    pipe_1_down = dict()
    pipe_2_down = dict()
    pipes_down = [pipe_1_down , pipe_2_down]
    pipe_1_down[pp_1_down.x] = pp_1_down
    pipe_1_down[pp_2_down.x] = pp_2_down
    pipe_1_down[pp_3_down.x] = pp_3_down
    pipe_1_down[pp_4_down.x] = pp_4_down
    pipe_1_down[pp_5_down.x] = pp_5_down
    pipe_2_down[pp_6_down.x] = pp_6_down
    pipe_2_down[pp_7_down.x] = pp_7_down
    pipe_2_down[pp_8_down.x] = pp_8_down
    pipe_1_up = dict()
    pipe_2_up = dict()
    pipes_up = [pipe_1_up, pipe_2_up]
    pipe_1_up[pp_1_up.x] = pp_1_up
    pipe_1_up[pp_2_up.x] = pp_2_up
    pipe_1_up[pp_3_up.x] = pp_3_up
    pipe_1_up[pp_4_up.x] = pp_4_up
    pipe_2_up[pp_5_up.x] = pp_5_up
    pipe_2_up[pp_6_up.x] = pp_6_up
    pipe_2_up[pp_7_up.x] = pp_7_up
    pipe_2_up[pp_8_up.x] = pp_8_up
    first_plane.add(superman)
    first_plane.add(sup)

    first_plane.add(heart_1)
    first_plane.add(heart_2)
    first_plane.add(heart_3)
    first_plane.add(heart_4)
    first_plane.add(heart_5)

    first_plane.add(number_1)
    first_plane.add(number_2)


    first_plane_portal.add(superman)
    #first_plane_portal.add(sup)
    i = 0
    x = 28
    while i < 15:
        grass = Sprite("grass.png", scale=0.8, position=(x, 25))
        first_plane.add(grass)
        i += 1
        x += 56
    i = 0
    x = 28
    while i < 15:
        metal_down = Sprite("metal_down.png", scale=0.8, position=(x, 25))
        metal_up = Sprite("metal_up.png", scale=0.8, position=(x, 475))
        first_plane_portal.add(metal_down)
        first_plane_portal.add(metal_up)
        i += 1
        x += 56
    first_plane_portal.add(heart_1)
    first_plane_portal.add(heart_2)
    first_plane_portal.add(heart_3)
    first_plane_portal.add(heart_4)
    first_plane_portal.add(heart_5)
    first_plane_portal.add(number_1)
    first_plane_portal.add(number_2)
    num_four = Sprite("hud_4.png", scale=0.7, position=(15, 462))
    num_zero = Sprite("hud_0.png", scale=0.7, position=(35, 462))
    cn_menu = Sprite("coins.png", scale=0.4, position=(60, 462), color=(255, 255, 0))
    first_plane_portal.add(num_four)
    first_plane_portal.add(num_zero)
    first_plane.add(num_four)
    first_plane.add(num_zero)
    first_plane.add(cn_menu)
    first_plane_portal.add(cn_menu)

    backgrounds[0].add(gr_1)                      # Земля в воздухе
    backgrounds[0].add(gr_2)
    backgrounds[0].add(gr_3)
    backgrounds[0].add(gr_4)
    backgrounds[0].add(gr_5)
    backgrounds[0].add(gr_6)
    backgrounds[0].add(gr_7)

    backgrounds[1].add(gr_8)
    backgrounds[1].add(gr_9)
    backgrounds[1].add(gr_10)
    backgrounds[1].add(gr_3_chocolate)
    backgrounds[1].add(gr_12)
    backgrounds[1].add(gr_4_chocolate)
    backgrounds[1].add(gr_5_chocolate)
    backgrounds[1].add(gr_14)

    backgrounds[1].add(sp_6)
    backgrounds[1].add(sp_7)
    backgrounds[1].add(sp_8)
    backgrounds[1].add(sp_9)
    backgrounds[1].add(sp_10)


    backgrounds[0].add(gr_1_chocolate)
    backgrounds[0].add(gr_2_chocolate)

    backgrounds[0].add(gr_danger_1)
    backgrounds[1].add(gr_danger_2)
    backgrounds[0].add(sp_1)                      # Шипы
    backgrounds[0].add(sp_2)
    backgrounds[0].add(sp_3)
    backgrounds[0].add(sp_4)
    backgrounds[0].add(sp_5)

    backgrounds[0].add(cn_1)                      # Монеты
    backgrounds[0].add(cn_2)
    backgrounds[0].add(cn_3)
    backgrounds[0].add(cn_4)
    backgrounds[0].add(cn_5)
    backgrounds[0].add(cn_31)
    backgrounds[0].add(cn_29)
    backgrounds[0].add(cn_30)

    backgrounds[3].add(cn_39)
    backgrounds[3].add(cn_40)
    backgrounds[3].add(cn_41)
    backgrounds[3].add(cn_42)
    backgrounds[3].add(cn_43)
    backgrounds[3].add(cn_44)


    backgrounds[1].add(cn_7)
    backgrounds[1].add(cn_8)
    backgrounds[1].add(cn_9)
    backgrounds[1].add(cn_10)
    backgrounds[1].add(cn_11)
    backgrounds[1].add(cn_12)

    backgrounds[0].add(bul_1)
    backgrounds[0].add(gn_1)

    backgrounds[2].add(bul_2)
    backgrounds[2].add(gn_2)
    backgrounds[2].add(bul_3)
    backgrounds[2].add(gn_3)
    backgrounds[2].add(bul_4)
    backgrounds[2].add(gn_4)
    backgrounds[2].add(bul_5)
    backgrounds[2].add(bul_6)
    backgrounds[2].add(gn_6)
    backgrounds[2].add(gn_5)
    backgrounds[2].add(gr_15)
    backgrounds[2].add(gr_16)
    backgrounds[2].add(gr_17)
    backgrounds[2].add(gr_18)
    backgrounds[2].add(gr_19)
    backgrounds[2].add(gr_18)
    backgrounds[2].add(gr_19)

    backgrounds[3].add(gr_20)
    backgrounds[3].add(gr_21)
    backgrounds[3].add(gr_22)
    backgrounds[3].add(gr_23)
    backgrounds[3].add(gr_24)
    backgrounds[3].add(gr_10_chocolate)
    backgrounds[3].add(gr_11_chocolate)
    backgrounds[3].add(gr_26)
    backgrounds[3].add(gr_13_chocolate)
    backgrounds[3].add(sp_14)
    backgrounds[3].add(sp_15)
    backgrounds[3].add(sp_16)
    backgrounds[3].add(sp_17)
    backgrounds[3].add(sp_18)
    backgrounds[3].add(sp_19)
    backgrounds[2].add(gr_6_chocolate)
    backgrounds[2].add(gr_7_chocolate)
    backgrounds[2].add(gr_8_chocolate)
    backgrounds[2].add(gr_9_chocolate)
    backgrounds[2].add(cn_33)
    backgrounds[2].add(cn_34)
    backgrounds[2].add(cn_35)
    backgrounds[2].add(cn_36)
    backgrounds[2].add(cn_37)
    backgrounds[2].add(cn_38)
    backgrounds[2].add(sp_11)
    backgrounds[2].add(sp_12)
    backgrounds[2].add(sp_13)

    backgrounds[0].add(en_1)                      # Враги
    backgrounds[1].add(en_2)
    backgrounds[2].add(en_3)
    backgrounds[0].add(brd_1)
    backgrounds[3].add(brd_2)
    backgrounds[3].add(brd_3)
    backgrounds[1].add(portal_1)

    backgrounds[0].add(sign_right)
    backgrounds[1].add(sign_right)
    backgrounds[2].add(sign_right)
    backgrounds[3].add(sign_exit)

    background_portal1.add(pp_1_down)
    background_portal1.add(pp_2_down)
    background_portal1.add(pp_3_down)
    background_portal1.add(pp_4_down)
    background_portal1.add(pp_5_down)
    background_portal1.add(pp_1_up)
    background_portal1.add(pp_2_up)
    background_portal1.add(pp_3_up)
    background_portal1.add(pp_4_up)
    background_portal1.add(cn_13)
    background_portal1.add(cn_14)
    background_portal1.add(cn_15)
    background_portal1.add(cn_16)
    background_portal1.add(cn_17)
    background_portal1.add(cn_18)
    background_portal1.add(cn_19)
    background_portal1.add(cn_20)


    background_portal2.add(pp_6_down)
    background_portal2.add(pp_7_down)
    background_portal2.add(pp_8_down)
    background_portal2.add(pp_5_up)
    background_portal2.add(pp_6_up)
    background_portal2.add(pp_7_up)
    background_portal2.add(pp_8_up)

    background_portal2.add(cn_21)
    background_portal2.add(cn_22)
    background_portal2.add(cn_23)
    background_portal2.add(cn_24)
    background_portal2.add(cn_25)
    background_portal2.add(portal_1_end)

    menu_scene=Scene()
    menu_scene_rus = Scene()
    background_menu =Layer()
    background_menu.add(picture)
    background_menu_rus = Layer()
    background_menu_rus.add(picture)

    back_rule=Layer()
    rl = Sprite('rules.png', position=(375, 250))
    back_rule.add(rl)


    menu_scene.add(background_menu)
    menu_scene.add(MultiplexLayer(
        MainMenu(),
        OptionsMenu(),
        ScoresLayer()))
    menu_scene_rus.add(background_menu_rus)
    menu_scene_rus.add(MultiplexLayer(
        MainMenu_rus(),
        OptionsMenu_rus(),
        ScoresLayer()))

    lose = Label(
        'YOU LOOSE(',
        font_name='PAPYRUS',
        font_size=32,
        anchor_x='center', anchor_y='center' ,position=(375 ,350) , color =(255 ,0 ,100, 255)
    )
    win = Label(
        'YOU WIN)',
        font_name='PAPYRUS',
        font_size=32,
        anchor_x='center', anchor_y='center', position=(375, 350), color=(255, 0, 100, 255)
    )
    bird = Label(
        'PRESS SPACE!',
        font_name='PAPYRUS',
        font_size=25,
        anchor_x='center', anchor_y='center', position=(310, 480), color=(255, 0, 100, 255)
    )

    main_scene.add(backgrounds[superman.fone])
    main_scene.add(first_plane)

    director.run(menu_scene)

if __name__ == '__main__':
    main()




